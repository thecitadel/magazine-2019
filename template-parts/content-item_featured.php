<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Citadel_Magazine_2019
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="background-image: url('<?php echo esc_url( the_post_thumbnail_url() ); ?>');">

	<a href="<?php echo esc_url( get_permalink() ) ?>" rel="bookmark">

		<div class="overlay"></div>

		<div class="content-container">

			<header class="entry-header">
				<?php the_title( '<h3 class="entry-title">', '<i class="fas fa-long-arrow-alt-right"></i></h3>' ) ?>

			</header><!-- .entry-header -->

			<?php if ( has_excerpt() ) : ?>
			<div class="entry-content">
				<?php echo the_excerpt(); ?>
			</div><!-- .entry-content -->
			<?php endif; ?>

			<footer class="entry-footer">
				<?php citadel_mag_2019_entry_footer(); ?>
			</footer><!-- .entry-footer -->

		</div>

	</a>

</article><!-- #post-<?php the_ID(); ?> -->
