<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Citadel_Magazine_2019
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<a href="<?php echo esc_url( get_permalink() ); ?>" class="post-thumbnail" style="background-image: url('<?php echo esc_url( the_post_thumbnail_url() ); ?>');"></a>

	<?php 

		$categories = get_the_category();
		$pillarsID = get_category_by_slug( 'pillars' );

		foreach ($categories as $category) {
			if ( ( in_category( 'pillars' ) ) && ( $category->name !== 'Pillars' ) ) {
				?>

				<div class="item-category"><?php echo $category->name; ?></div>

				<?php
			}
		}

	?>

	<header class="entry-header">
		<?php the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ) ?>
	</header><!-- .entry-header -->

	<?php if ( has_excerpt() ) : ?>
	<div class="entry-content">
		<?php echo the_excerpt(); ?>
	</div><!-- .entry-content -->
	<?php endif; ?>

	<footer class="entry-footer">
		<?php citadel_mag_2019_entry_footer(); ?>
	</footer><!-- .entry-footer -->

</article><!-- #post-<?php the_ID(); ?> -->
