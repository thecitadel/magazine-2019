<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Citadel_Magazine_2019
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header" style="background-image: url('<?php echo the_post_thumbnail_url(); ?>');">
		<div class="entry-header-container">
			<?php
			
			if( get_field('featured_title_image') ):

			?>

				<img src="<?php the_field('featured_title_image'); ?>" alt="" role="presentation" class="<?php the_field('featured_title_image_orientation'); ?>" />

			<?php

				the_title( '<h1 class="entry-title screen-reader-text">', '</h1>' );

			else :

				the_title( '<h1 class="entry-title">', '</h1>' );

			endif;

			if ( 'post' === get_post_type() ) :
				?>
				<div class="entry-meta">
					<?php if ( has_excerpt() ) : ?>
						<div class="mag-deck"><?php the_excerpt(); ?></div>
					<?php endif; ?>
					<?php if( get_field('magazine_byline') ): ?>
						<p class="byline"><span>by</span> <?php the_field('magazine_byline'); ?><?php if( get_field('photography_byline') ): ?> <span class="sep">|</span> <span>photography by</span> <?php the_field('photography_byline'); ?><?php endif; ?></p>
					<?php endif; ?>
					
				</div><!-- .entry-meta -->
			<?php endif; ?>
		</div>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<div class="entry-content-container">

			<?php if ( has_excerpt() ) : ?>
				<div class="mobile-mag-deck"><em><?php the_excerpt(); ?></em></div>
			<?php endif; ?>

			<?php
			the_content( sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'citadel-mag-2019' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'citadel-mag-2019' ),
				'after'  => '</div>',
			) );
			?>
		</div>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php citadel_mag_2019_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
