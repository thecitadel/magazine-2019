<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Citadel_Magazine_2019
 */

?>

	</div><!-- #content -->

	<?php if ( is_home() || is_front_page() ) : ?>
	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<?php
				printf( esc_html__( '&copy; %1$s. All rights reserved.', 'citadel-mag-2019' ), '<a href="https://citadel.edu/">The Citadel</a>' );
			?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
	<?php endif; ?>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
