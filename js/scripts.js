jQuery(document).ready(function($) {

	$('.menu-toggle').click(function() {

		$('body').addClass('menu-visible');
		$('#masthead').addClass('menu-visible');
		$('.main-navigation').addClass('toggled');

	});

	$('.menu-close').click(function() {

		$('body').removeClass('menu-visible');
		$('#masthead').removeClass('menu-visible');
		$('.main-navigation').removeClass('toggled');

	});

});