<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Citadel_Magazine_2019
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'citadel-mag-2019' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">
			<?php
			
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title-mag">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="">

						<img src="<?php echo get_template_directory_uri(); ?>/images/The-Citadel-Magazine-Nameplate_White.png" alt="The Citadel Magazine <?php bloginfo( 'name' ); ?>">
							
					</a>
				</h1>
				<?php
			else :
				?>
				<p class="site-title-mag">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">

						<img src="<?php echo get_template_directory_uri(); ?>/images/The-Citadel-Magazine-Nameplate_White.png" alt="The Citadel Magazine <?php bloginfo( 'name' ); ?>">
					
					</a>
				</p>
				<?php
			endif;
			?>
		</div><!-- .site-branding -->
		<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><span class="screen-reader-text">Primary Menu</span><i class="fas fa-ellipsis-v"></i></button>
	</header><!-- #masthead -->

	<nav id="site-navigation" class="main-navigation">

		<div class="full-menu">
			<button class="menu-close" aria-controls="primary-menu" aria-expanded="true"><span class="screen-reader-text">Primary Menu close</span><i class="fas fa-times"></i></button>
		<?php

		$args = array(
			'site__not_in' => 1,
		);

		$subsites = get_sites($args);

		if ( ! empty ( $subsites ) ) {

			echo '<ul class="subsites nav-menu"><li><h3>Digital Issues</h3></li>';

			foreach( $subsites as $subsite ) {

				$subsite_id = get_object_vars( $subsite )["blog_id"];
				$subsite_name = get_blog_details( $subsite_id )->blogname;
				$subsite_link = get_blog_details( $subsite_id )->siteurl;
				echo '<li class="site-' . $subsite_id . '"><a href="' . $subsite_link . '">' . $subsite_name . '</a></li>';
				
			}
			
			echo '<li><a href="' . network_site_url() . '?p=previous">Previous Issues</a></li></ul>';
		
		}

		?>

		</div>

	</nav><!-- #site-navigation -->

	<div id="content" class="site-content">
