<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Citadel_Magazine_2019
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			$next_post = get_next_post();
		    $previous_post = get_previous_post();

		    if ( $next_post && $previous_post ) :
			    the_post_navigation( array(
			        'next_text' => '<span class="screen-reader-text">' . __( 'Next post:', 'citadel-mag-2019' ) . '</span> ' .
			            '<span class="post-title">%title<i class="fas fa-long-arrow-alt-right"></i></span>' . get_the_post_thumbnail($next_post->ID,'full'),
			        'prev_text' => '<span class="screen-reader-text">' . __( 'Previous post:', 'citadel-mag-2019' ) . '</span> ' .
			            '<span class="post-title"><i class="fas fa-long-arrow-alt-left"></i>%title</span>' . get_the_post_thumbnail($previous_post->ID,'full'),
			    ) );
			elseif ( $next_post ) :
				the_post_navigation( array(
			        'next_text' => '<span class="screen-reader-text">' . __( 'Next post:', 'citadel-mag-2019' ) . '</span> ' .
			            '<span class="post-title">%title<i class="fas fa-long-arrow-alt-right"></i></span>' . get_the_post_thumbnail($next_post->ID,'full'),
			    ) );
			elseif ( $previous_post ) :

				the_post_navigation( array(
			        'prev_text' => '<span class="screen-reader-text">' . __( 'Previous post:', 'citadel-mag-2019' ) . '</span> ' .
			            '<span class="post-title"><i class="fas fa-long-arrow-alt-left"></i>%title</span>' . get_the_post_thumbnail($previous_post->ID,'full'),
			    ) );

			endif;


		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
