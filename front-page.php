<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Citadel_Magazine_2019
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php

			// Featured

			$args = array (
				'posts_per_page' 	=> 3,
				'category_name' 	=> 'featured',
				'orderby'			=> 'rand',
				'order'				=> 'ASC', 
			);

			$the_query = new WP_Query( $args );

			if ( $the_query->have_posts() ) {

				?>

				<section id="featured">

					<h2 class="screen-reader-text">Featured Stories</h2>

					<?php

					while ( $the_query->have_posts() ) {

						$the_query->the_post();

						get_template_part( 'template-parts/content', 'item_featured' );

					}

					?>

				</section>

				<?php

			}

			wp_reset_postdata();

			?>

			<div class="homepage-sections">

			<?php

			// Pillars

			$args = array (
				'posts_per_page' 	=> -1,
				'category_name' 	=> 'pillars',
				'orderby'			=> 'rand',
				'order'				=> 'ASC', 
			);

			$the_query = new WP_Query( $args );

			if ( $the_query->have_posts() ) {

				?>

				<section id="pillars">

					<div class="section-info">
						<h2>The Four Pillars</h2>

						<?php 

						$cat_id = get_cat_ID ( 'Pillars' );
						echo category_description($cat_id);

						?>
						
					</div>

					<?php

					while ( $the_query->have_posts() ) {

						$the_query->the_post();

						get_template_part( 'template-parts/content', 'item' );

					}

					?>

				</section>

				<?php

			}

			wp_reset_postdata();

			?>

			<?php

			// Snapshot

			$args = array (
				'posts_per_page' 	=> -1,
				'category_name' 	=> 'snapshot',
				'orderby'			=> 'rand',
				'order'				=> 'ASC', 
			);

			$the_query = new WP_Query( $args );

			if ( $the_query->have_posts() ) {

				?>

				<section id="snapshot">

					<div class="section-info">
						<h2>Snapshot</h2>

						<?php 

						$cat_id = get_cat_ID ( 'Snapshot' );
						echo category_description($cat_id);

						?>
						
					</div>

					<?php

					while ( $the_query->have_posts() ) {

						$the_query->the_post();

						get_template_part( 'template-parts/content', 'item' );

					}

					?>

				</section>

				<?php

			}

			wp_reset_postdata();

			?>

			<?php

			// Other

			$args = array (
				'posts_per_page' 	=> -1,
				'category_name' 	=> 'other',
				'orderby'			=> 'rand',
				'order'				=> 'ASC', 
			);

			$the_query = new WP_Query( $args );

			if ( $the_query->have_posts() ) {

				?>

				<section id="other">

					<div class="section-info">
						<h2>Also in this Issue</h2>

						<?php 

						$cat_id = get_cat_ID ( 'Other' );
						echo category_description($cat_id);

						?>
						
					</div>

					<?php

					while ( $the_query->have_posts() ) {

						$the_query->the_post();

						get_template_part( 'template-parts/content', 'item' );

					}

					?>

				</section>

				<?php

			}

			wp_reset_postdata();

			?>

			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
